import unittest

from compression.arithmetic_coding_compressor import (arithmetic_coding_dictionary,
                                                      arithmetic_coding_compress,
                                                      arithmetic_coding_dictionary_bytes,
                                                      arithmetic_coding_decompress)
from compression.canonical_huffman import (canonical_compress,
                                           canonical_decompress,
                                           canonical_dictionary,
                                           canonical_dictionary_bytes)
from compression.common import load_text, byte_count
from compression.markov_compressor import (markov_dictionary, markov_compress,
                                           markov_decompress, markov_dictionary_bytes)

original = {
    'english': load_text("tests/english.txt"),
    'french': load_text("tests/french.txt"),
    'chinese': load_text('tests/chinese.txt'),
    'russian': load_text('tests/russian.txt')
}
all_languages = [x for sublist in original.values() for x in sublist]
original_len = sum([byte_count(x) for x in all_languages])


class TestCompress(unittest.TestCase):

    def setUp(self):
        pass

    def __test_compression(self, name, dict_generator, compressor, decompressor, dict_bytes_calculator):
        # Some compressors may have dependencies between calls to compress and decompress, so
        # create one compressor per file, though all based on all the languages
        codecs = {k: dict_generator(all_languages) for k in original.keys()}
        symbol_codec = codecs['english']
        compressed_files = {k: compressor(codecs[k], v) for k, v in original.items()}
        codec_bytes = dict_bytes_calculator(symbol_codec)
        compressed_len = codec_bytes + sum([len(x) for x in compressed_files.values()])
        print('\nCompressor: {}'.format(name))
        print('Original: {}. Codec: {}'.format(original_len, codec_bytes))
        for k, v in compressed_files.items():
            print('{}: {}'.format(k, len(v)))
        print('Compressed Total: {}'.format(compressed_len))
        print('Compression Ratio: {}'.format(compressed_len / original_len))

        decompressed_files = {k: decompressor(codecs[k], v) for k, v in compressed_files.items()}
        for k, v in decompressed_files.items():
            self.assertEqual(original[k], v, msg='Decompression comparison failed for file {}'.format(k))

    def test_canonical_compress(self):
        self.__test_compression('canonical_huffman', canonical_dictionary, canonical_compress, canonical_decompress,
                                canonical_dictionary_bytes)

    def test_arithmetic_compress(self):
        self.__test_compression('arithmetic_coding', arithmetic_coding_dictionary, arithmetic_coding_compress,
                                arithmetic_coding_decompress, arithmetic_coding_dictionary_bytes)

    def __test_markov_n(self, context_depth):
        def markov_n_dict(data):
            return markov_dictionary(data, context_depth)

        self.__test_compression('markov-{}'.format(context_depth), markov_n_dict, markov_compress, markov_decompress,
                                markov_dictionary_bytes)

    def test_markov_1(self):
        self.__test_markov_n(1)

    def test_markov_2(self):
        self.__test_markov_n(2)

    def test_markov_3(self):
        self.__test_markov_n(3)


if __name__ == '__main__':
    unittest.main()
