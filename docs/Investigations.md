
##Some initial reading

1. [Prediction by Partial Matching](https://github.com/nayuki/Reference-arithmetic-coding/blob/master/python/ppm-compress.py), 
2. [Longest common substring](https://pypi.org/project/pylcs/)
3. Huffman implementations: https://github.com/nicktimko/huffman, https://github.com/ilanschnell/bitarray (pip install bitarray), https://stackoverflow.com/questions/33089660/decoding-a-huffman-code-with-a-dictionary, https://github.com/jerabaul29/python_huffman, https://github.com/soxofaan/dahuffman (python 3.5+).
4. A simple, but expensive, algorithm to extract the most common substrings can be found [here](https://stackoverflow.com/questions/14670632/algorithm-to-find-the-most-common-substrings-in-a-string). 

The above will be investigated and compared to see what "off the shelf" code can achieve.

## Results

### Huffman
The test set of documents in tests/*.txt were compressed using a straightforward Huffman library. The test files comprise an English language extract from wikipedia, plus a number of automated translations using Google Translate, into French, Chinese and Russian. This produces the following results:
```
Analysing compression scheme "Huffman"
Original: 4687. Codec: 1750.0, Compressed Total: 4164.0, Compressed Ratio: 0.8884147642415191
Filename: chinese.txt, Original: 678, Compressed: 300, Ratio: 0.4424778761061947
Filename: english.txt, Original: 968, Compressed: 620, Ratio: 0.640495867768595
Filename: french.txt, Original: 1191, Compressed: 727, Ratio: 0.6104114189756508
Filename: russian.txt, Original: 1850, Compressed: 767, Ratio: 0.4145945945945946
Filename: all_file_data, Original: 4687, Compressed: 2412, Ratio: 0.5146148922551739
```
The symbol table is recorded [here](HuffmanSymbolTable.md).

This last line attempts to quantify how much memory is required to store the compressed data + the dictionary in ROM. However, this is probably an underestimate and/or more space will be required in RAM to make the reverse lookup table. A processing efficient version may need to use a hashmap for the lookup, or we may be able to make some more memory efficient data structure for the lookup at the expense of programming time.

### CSCv1
Huffman coding has just squeezed the entropy out of the individual symbols. However, in a natural text there will be repeated substrings across all the text, even across languages, especially in this case of parallel translations, so the next stage tries to extract more of this predictability out of the data. Offline processing of the data is a possibility, so no attempt is yet made to make this efficient in terms of CPU time. The algorithm searches through for various length substrings (currently up to about 20 characters as a reasonable starting point as a tradeoff between savings and search time) and find the top candidate for saving space, using the metric of (frequency - 1) * size in bytes (NOT in characters). This top candidate is removed from the string, creating substrings around the holes thus created. This list of substrings goes through the same process of taking out the next top candidate. This process repeats until there's no text left, or the candidates are no longer worth removing according to some metric. That metric is yet to be determined, but it needs to be such that replacing the occurences with a minimal code + associated overhead + entry in codec/dictionary < the size of just not removing the larger symbols at all (i.e., freq * bytes).

In Python-esque pseudocode, this is
```python
remaining_string_list = [data_to_compress]
SubstringRecord = namedtuple('SubstringRecord', 'str, freq')
substrings_to_remove = []
while remaining_string_list:
  # get the candidates for removal sorted by (frequency - 1) * bytes, largest first
  sorted_candidates = get_sorted_candidates(remaining_string_list)
  if metric(sorted_candidates[0]) < MIN_VIABLE_SUBSTITUTION_SIZE:
    return substrings_to_remove
  remaining_string_list = remove_substring(sorted_candidates[0], remaining_string_list)
  substrings_to_remove.append(SubstringRecord('str'=sorted_candidates[0], freq=freq)
return substrings_to_remove
```
Whatever's left at this stage is counted as individual symbols. The frequencies of all these symbols (multi-char strings and single char) are determined and used to generate a huffman code.

[This algorithm, called CSCv1](https://gitlab.com/crnosrce/closed-set-compressor/-/commit/671f944708d28a4c2c36020cbcdba5d5d38397bc), had the following results:
```
Analysing compression scheme "CSCv1"
Original: 4687. Codec: 1452, Compressed Total: 3967, Compressed Ratio: 0.8463836142521869
Filename: chinese.txt, Original: 678, Compressed: 282, Ratio: 0.415929203539823
Filename: english.txt, Original: 968, Compressed: 652, Ratio: 0.6735537190082644
Filename: french.txt, Original: 1191, Compressed: 773, Ratio: 0.6490344248530646
Filename: russian.txt, Original: 1850, Compressed: 808, Ratio: 0.43675675675675674
Filename: all_file_data, Original: 4687, Compressed: 2513, Ratio: 0.536163857478131
```
The symbol table is recorded [here](CSCv1SymbolTable.md).

Obviously, this is not a great result. During the develop of CSCv2 a bug was discovered which means the above result is much larger than it should have been. This version re-used the huffman compressor which only looks for single symbols at a time. This bug was fixed in v2.

###CSCv2 
A small tweak to try for CSC will be to change the metric to count the bits of a substitution. However, since compression time isn't important, we can go further and try a symbol substitution, perform the full compression and then measure the result. If that substitution isn't better than the best so far, we don't proceed with it. Each stage needs to be better than the previous best in order to progress. This is a greedy optimisation, but at least it avoids the issues in CSCv1. Implementation and results for this approach are pending.

Results for this approach are:
```
Original: 4687. Codec: 1272, Compressed Total: 3483, Compressed Ratio: 0.7431192660550459
Filename: chinese.txt, Original: 678, Compressed: 285, Ratio: 0.42035398230088494
Filename: english.txt, Original: 968, Compressed: 545, Ratio: 0.5630165289256198
Filename: french.txt, Original: 1191, Compressed: 649, Ratio: 0.5449202350965575
Filename: russian.txt, Original: 1850, Compressed: 732, Ratio: 0.3956756756756757
Filename: all_file_data, Original: 4687, Compressed: 2210, Ratio: 0.47151696180925967
```
The code for this version is [here](https://gitlab.com/crnosrce/closed-set-compressor/-/commit/83951a60bc1df7bbc7f6415ce01146c654c6dc93).

This is a fairly complete estimate of the space required for a decompressor. The code will be fairly minimal and RAM costs negligible. Also, for larger data the dictionary will only grow sub-linearly with the size of data to be compressed so for a larger number of strings input the ratio should look better. Still, it seems that the ratio is not that great. Just bear in mind that when comparing with other solutions is that this is (almost) the total size in ROM. To compare against other options, the compiled size of the decompressor for that method needs to be known. Also, RAM usage in other schemes is generally much larger (relative to a small/fully utilised embedded memory, that is).