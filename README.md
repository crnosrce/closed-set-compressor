# Closed Set Compressor

This is a playground for investigation into ideas for compressing a fixed, known and closed set of strings maximally.

The requirements are:

* the set of strings is fixed (closed) and known in advance
* each string can be decompressed individually
* decompression time must be fast and low-memory costs
* compression time is not important. If several hours of computation time produce better compression, that would be chosen.

Some [investigations and musings](docs/Investigations.md) for those who are interested.

A docker environment is available for consistency, however it should be easy to set up for another python environment. If not using docker, I recommend pyenv + virtualenv. Requirements are in the docker\requirements.txt.