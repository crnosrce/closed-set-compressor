import argparse
from math import ceil

from compression.arithmetic_coding_compressor import (arithmetic_coding_dictionary,
                                                      arithmetic_coding_compress,
                                                      arithmetic_coding_dictionary_bytes,
                                                      arithmetic_coding_decompress)
from compression.canonical_huffman import (canonical_dictionary,
                                           canonical_dictionary_bytes,
                                           canonical_compress,
                                           canonical_decompress)
from compression.common import (ALL_DATA_KEY,
                                byte_count,
                                load_files,
                                calculate_markov_mapping)
from compression.markov_compressor import (markov_dictionary,
                                           markov_compress,
                                           markov_dictionary_bytes,
                                           markov_decompress)


def analyse_data(data_to_compress):
    print("\nData Analysis: ")
    for name, data_list in data_to_compress.items():
        num_chars = sum([len(data_item) for data_item in data_list])
        symbols = set([x for data_item in data_list for x in data_item])
        markov_tables = \
            {markov_order: calculate_markov_mapping(markov_order, symbols, data_list) for markov_order in range(1, 6)}
        print("{}: Num characters (n) = {}. Num symbols (S) = {}".format(name, num_chars, len(symbols)))
        for markov_order, markov_table in markov_tables.items():
            num_markov_entries = len(markov_table.values())
            average_chain_length = sum([len(x) for x in markov_table.values()]) / num_markov_entries
            print("Markov-{} table contains {} entries of average length {}".format(markov_order,
                                                                                    num_markov_entries,
                                                                                    average_chain_length))


def __analyse_compression(name, data_to_compress, codec_generator, compressor,
                          dict_size_calculator, decompressor, codec_estimate_scale=1.5,
                          print_symbols=True):
    def to_list(data):
        return data if isinstance(data, list) else [data]

    print('\nAnalysing compression scheme "{}"'.format(name))
    data_to_compress = {k: to_list(v) for k, v in data_to_compress.items()}
    # the input is a dictionary of 'filename': text items
    all_languages = data_to_compress[ALL_DATA_KEY]
    all_languages_len = byte_count(''.join(all_languages))
    print('Generating codecs...')
    # Some compressors may have dependencies between calls to compress and decompress, so
    # create one compressor per file, though all based on all the languages
    codecs = {k: codec_generator(all_languages) for k in data_to_compress.keys()}
    if print_symbols:
        codecs[ALL_DATA_KEY].print_code_table()
        # for filename, codec in {k: codec_generator(data_to_compress[k]) for k in data_to_compress.keys()}.items():
        #     print("{} has codec size {}".format(filename, codec.calc_size()))
    else:
        print('Codecs done...')
    compressed_data = {k: compressor(codecs[k], v) for k, v in data_to_compress.items()}
    codec_bytes = dict_size_calculator(list(codecs.values())[0])
    data_compressed_len = len(compressed_data[ALL_DATA_KEY])
    total_compressed_len = codec_bytes + data_compressed_len
    compression_ratio = total_compressed_len / all_languages_len
    print('Original: {}. Codec: {}, Compressed Total: {}, Compressed Ratio: {}'.format(all_languages_len,
                                                                                       codec_bytes,
                                                                                       total_compressed_len,
                                                                                       compression_ratio))
    # just some estimates for a bigger data set
    estimated_large_compression_ratio_excluding_codec = data_compressed_len / all_languages_len
    estimated_codec_bytes = ceil(codec_bytes * codec_estimate_scale)
    large_data_size = 250 * 1024
    estimated_large_compression = ceil(
        estimated_large_compression_ratio_excluding_codec * large_data_size + estimated_codec_bytes)
    estimated_large_compression_ratio = estimated_large_compression / large_data_size
    print('Estimated for 250kb of data - Codec: {}, Compressed Total: {}kb, Compressed Ratio: {}'.format(
        estimated_codec_bytes,
        estimated_large_compression // 1024,
        estimated_large_compression_ratio))
    for k, v in compressed_data.items():
        data = data_to_compress[k]
        original_size = byte_count(''.join(data) if isinstance(data, list) else data)
        compressed_size = len(v)
        individual_compression_ratio = compressed_size / original_size
        print('Filename: {}, Original: {}, Compressed: {}, Ratio: {}'.format(k,
                                                                             original_size,
                                                                             compressed_size,
                                                                             individual_compression_ratio))
    # confirm that decompression is correct
    for k, v in compressed_data.items():
        decompressed = decompressor(codecs[k], v)
        matches = data_to_compress[k] == decompressed
        print('Filename: {} {} the original after decompressing'.format(k, 'matches' if matches else 'is different to'))


def analyse_canonical(data_to_compress):
    __analyse_compression(name='Canonical Huffman', data_to_compress=data_to_compress, codec_generator=canonical_dictionary,
                          compressor=canonical_compress, dict_size_calculator=canonical_dictionary_bytes,
                          decompressor=canonical_decompress)


def analyse_arithmetic(data_to_compress):
    __analyse_compression(name='Arithmetic Coding', data_to_compress=data_to_compress, codec_generator=arithmetic_coding_dictionary,
                          compressor=arithmetic_coding_compress, dict_size_calculator=arithmetic_coding_dictionary_bytes,
                          decompressor=arithmetic_coding_decompress)


def analyse_markov(data_to_compress, context_depth):
    def dict_generator(data):
        return markov_dictionary(data, context_depth)

    __analyse_compression(name='Markov-{}'.format(context_depth), data_to_compress=data_to_compress,
                          codec_generator=dict_generator, compressor=markov_compress,
                          dict_size_calculator=markov_dictionary_bytes,
                          decompressor=markov_decompress)


def get_parsed_args():
    parser = argparse.ArgumentParser(
        description='Processes all images found in the source path to the destination path')
    parser.add_argument('--languages_dir', '-l', default='tests', type=str,
                        help='the source directory for the language files')
    parser.add_argument('--csv', '-c', action='store_true',
                        help='read CSV files instead of TXT files from the languages_dir')
    return parser.parse_args()


def main():
    args = get_parsed_args()
    languages_dir = args.languages_dir
    read_csvs = args.csv
    loaded_files = load_files(languages_dir, read_csvs)
    analyse_data(loaded_files)
    # analyse_dahuffman(loaded_files)
    analyse_canonical(loaded_files)
    analyse_arithmetic(loaded_files)
    for i in range(3):
        analyse_markov(loaded_files, i + 1)


if __name__ == '__main__':
    main()
