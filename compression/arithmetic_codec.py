import collections
import contextlib

from compression.arithmeticcoding import (FrequencyTable,
                                          ArithmeticEncoder,
                                          ArithmeticDecoder,
                                          BitInputStream,
                                          BitOutputStream)
from compression.common import (calc_indexed_symbols_bytes,
                                bits_for,
                                bytes_needed_for_num_bits)

CODE_VALUE_BITS = 32


class IndexedSymbolsFrequencyTable(FrequencyTable):

    def __init__(self, symbols, cumulative_frequencies):
        self.symbols = symbols
        self.symbols_index_map = {k: i for i, k in enumerate(self.symbols)}
        self.cumulative_frequencies = cumulative_frequencies

    # Returns the number of symbols in this frequency table, which is a positive number.
    def get_symbol_limit(self):
        return len(self.symbols)

    # Returns the frequency of the given symbol. The returned value is at least 0.
    def get(self, symbol):
        raise NotImplementedError()

    # Sets the frequency of the given symbol to the given value.
    # The frequency value must be at least 0.
    def set(self, symbol, freq):
        raise NotImplementedError()

    # Increments the frequency of the given symbol.
    def increment(self, symbol):
        raise NotImplementedError()

    # Returns the total of all symbol frequencies. The returned value is at
    # least 0 and is always equal to get_high(get_symbol_limit() - 1).
    def get_total(self):
        return self.cumulative_frequencies[-1]

    # Returns the sum of the frequencies of all the symbols strictly
    # below the given symbol value. The returned value is at least 0.
    def get_low(self, symbol_idx):
        return self.cumulative_frequencies[symbol_idx]

    # Returns the sum of the frequencies of the given symbol
    # and all the symbols below. The returned value is at least 0.
    def get_high(self, symbol_idx):
        return self.cumulative_frequencies[symbol_idx + 1]


class ByteStream(object):

    def __init__(self, byte_list=None):
        self.byte_list = byte_list if byte_list else []
        self.current_byte_to_read = 0
        self.finished_read = False

    def write(self, byte):
        self.byte_list.append(byte)

    def read(self, num_to_read):
        assert num_to_read == 1
        if self.current_byte_to_read < len(self.byte_list):
            result = [ord(self.byte_list[self.current_byte_to_read])]
            self.current_byte_to_read += num_to_read
        else:
            self.finished_read = True
            result = []
        return result

    def get_bytes(self):
        return self.byte_list

    def all_read(self):
        return self.finished_read

    def close(self):
        pass


class ArithmeticCodec(object):

    def __init__(self, symbols, cumulative_frequencies):
        self.freqs = IndexedSymbolsFrequencyTable(symbols, cumulative_frequencies)
        self.expected_num_results = 0  # WARNING! HACK!
        self.compressed_bits_map = {}

    def calc_size(self):
        # to decompress we need the symbols and the cumulative frequencies only
        # The symbols will be stored grouped by byte count and so we can use the
        # index from the cumulative frequency table in the function SBO() to find
        # the actual symbol, so no symbol offset table is required
        if isinstance(self.freqs.symbols[0], int):
            # assume they're all contiguous ints (common case)
            # in this case, we don't need to store the symbols separately
            # at all, as long as the cumulative frequencies are in this contiguous
            # order and we know the starting value, so the number of bytes for
            # storing the symbols is therefore zero
            num_bytes_for_symbols = 0
        else:
            num_bytes_for_symbols, _ = \
                calc_indexed_symbols_bytes({symbol: 0 for symbol in self.freqs.symbols})
        cumulative_frequencies_bits_per_entry = bits_for(self.freqs.cumulative_frequencies[-1])
        total_bits_for_cumulative_frequencies = \
            len(self.freqs.cumulative_frequencies) * cumulative_frequencies_bits_per_entry
        bytes_for_cumulative_frequencies = bytes_needed_for_num_bits(total_bits_for_cumulative_frequencies)
        return num_bytes_for_symbols + bytes_for_cumulative_frequencies

    def print_code_table(self, verbose=False):
        def format_symbol(symbol):
            if symbol == '\n':
                return '\\n'
            elif symbol == '\0':
                return '\\0'
            else:
                return symbol

        print("Number of Symbols: {}".format(len(self.freqs.symbols)))
        if verbose:
            print("Symbols: ")
            for idx, symbol in enumerate(self._symbols):
                print("{}: '{}'".format(idx, format_symbol(symbol)))
        print(self.freqs.cumulative_frequencies)

    def encode(self, data_list):
        self.expected_num_results = len(data_list)
        self._compressed_bitcount = 0
        compressed_data = ByteStream()
        with contextlib.closing(BitOutputStream(compressed_data)) as bitout:
            encoder = ArithmeticEncoder(32, bitout)  # this should be per string for random access
            for data_item in data_list:
                bits_at_start = bitout.total_bits_written
                # TODO: This is required to be able to access the strings randomly
                # but we would then need to record the length in bits of every string
                # so for now this is removed.
                # encoder = ArithmeticEncoder(32, bitout)
                for char in data_item:
                    encoder.write(self.freqs, self.freqs.symbols_index_map[char])
                bits_at_end = bitout.total_bits_written
                self.compressed_bits_map[data_item] = bits_at_end - bits_at_start
            encoder.finish()  # Flush remaining code bits
        return compressed_data.get_bytes()

    def decode(self, compressed_data):
        result = []
        compressed_byte_stream = ByteStream(compressed_data)
        bitin = BitInputStream(compressed_byte_stream)
        decoder = ArithmeticDecoder(32, bitin)
        curr_string = ''
        while len(result) < self.expected_num_results:
            symbol_idx = decoder.read(self.freqs)
            if symbol_idx == -1:
                break
            symbol = self.freqs.symbols[symbol_idx]
            curr_string += symbol
            if symbol == '\0':
                result.append(curr_string)
                curr_string = ''
                # the decoder needs to reset for each string to be randomly accessible
                # TODO: HOWEVER, THIS BREAKS DECOMPRESSION! We would need to record the start and bit count
                # of every word to be able to tell the byte stream when to finish properly so that
                # we read the last few characters correctly in small strings. This is possible but requires
                # more memory in the infrastructure to access individual strings. The number of bits in larger
                # strings would need 7 or 8 bits to specify, but we could remove the '\0' character from the end
                # of every string. However, per string, this is probably a 2 or 3 bit impost so, unless the encoding
                # is much worse because of the presence of the null terminators, it seems that using arithmetic coding
                # would increase storage requirements per word identifier, while reducing it by a smaller amount per
                # unique word, which seems like a loss overall.
                # decoder = ArithmeticDecoder(32, bitin)
        if curr_string:
            result.append(curr_string)
        return result

    def get_compressed_bitcount(self):
        return self._compressed_bitcount

    @classmethod
    def from_frequencies(cls, frequencies):
        cumulative_frequencies = [0 for _ in range(len(frequencies.keys()) + 1)]
        for idx, (symbol, frequency) in enumerate(frequencies.items()):
            cumulative_frequencies[idx + 1] = frequency + cumulative_frequencies[idx]
        return cls(list(frequencies.keys()), cumulative_frequencies)

    @classmethod
    def from_data(cls, data):
        """
        Build Arithmetic Coding range table from symbol sequence

        :param data: sequence of symbols (e.g. byte string, unicode string, list, iterator)
        :return: ArithmeticCoder
        """
        if isinstance(data, list):
            data_to_count = [x for sublist in data for x in sublist]
        else:
            data_to_count = data
        frequencies = collections.Counter(data_to_count)
        return cls.from_frequencies(frequencies)
