import collections
from heapq import heappush, heappop, heapify

from compression.common import (bytes_needed_for_num_bits,
                                calc_indexed_symbols_bytes,
                                get_bits_in_range,
                                get_symbols_in_left_to_right_order,
                                byte_count, bits_for)


def _guess_concat(data):
    """
    Guess concat function from given data
    """
    return {
        type(u''): u''.join,
        type(b''): bytes,
    }.get(type(data), list)


class CanonicalCodec(object):
    """
    The CanonicalCodec is quite different to the non-canonical codec,
    both in what it stores and how it decodes. Encoding is, however,
    pretty much the same. See https://en.wikipedia.org/wiki/Canonical_Huffman_code,
    https://books.google.com.au/books?id=UlzWDAAAQBAJ&pg=PA28&lpg=PA28&dq=a+canonical+Huffman+tree+can+be+stored+in+a+compact+representation+using+%7C%CE%A3%7C+log+%7C%CE%A3%7C+%2B+O(log%C2%B2+n)+bits&source=bl&ots=jQnI6RREGX&sig=ACfU3U2bjFpkjhFRmbA5DbuNM4MsjvNgnA&hl=en&sa=X&ved=2ahUKEwibtuOJtJTqAhX9wzgGHcE6CN4Q6AEwAHoECAoQAQ#v=onepage&q=a%20canonical%20Huffman%20tree%20can%20be%20stored%20in%20a%20compact%20representation%20using%20%7C%CE%A3%7C%20log%20%7C%CE%A3%7C%20%2B%20O(log%C2%B2%20n)%20bits&f=false,
    and https://www.geeksforgeeks.org/canonical-huffman-coding/.
    """

    def __init__(self, non_canonical_code_table, concat=list):
        def get_code_size(symbol):
            return non_canonical_code_table[symbol][0]

        def sort_key(symbol):
            return non_canonical_code_table[symbol][0], non_canonical_code_table[symbol][1]

        self.compressed_bits_map = {}
        self._compressed_bitcount = 0
        self._concat = concat
        self._code_table = dict(non_canonical_code_table)
        # Symbols in left to right order, Known as L in the literature
        self.symbols_left_to_right = sorted(get_symbols_in_left_to_right_order(self._code_table), key=sort_key)
        self._assign_canonical_codes(self.symbols_left_to_right, self._code_table)
        self.tree_height = max([bitsize for bitsize, _ in self._code_table.values()])
        # Known as F in the literature
        self.first_symbol_index_of_length = [-1 for _ in range(self.tree_height)]
        # Known as C in the literature
        self.first_symbol_code_of_length = [-1 for _ in range(self.tree_height)]
        for i in range(len(self.symbols_left_to_right) - 1, -1, -1):
            symbol = self.symbols_left_to_right[i]
            symbol_code_size = get_code_size(symbol)
            symbol_code_size_index = symbol_code_size - 1
            self.first_symbol_index_of_length[symbol_code_size_index] = i
            self.first_symbol_code_of_length[symbol_code_size_index] = self._code_table[symbol][1]
        for i in range(self.tree_height - 1, -1, -1):
            if self.first_symbol_index_of_length[i] == -1:
                self.first_symbol_index_of_length[i] = self.first_symbol_index_of_length[i + 1]
                self.first_symbol_code_of_length[i] = int(self.first_symbol_code_of_length[i + 1] / 2)

    def _assign_canonical_codes(self, symbols_left_to_right, code_table):
        def get_code(symbol):
            return code_table[symbol][1]

        def set_code(symbol, value):
            old_tuple = code_table[symbol]
            code_table[symbol] = (old_tuple[0], value)

        def get_size(symbol):
            return code_table[symbol][0]

        # the code_table values are (bitsize, value) tuples
        set_code(symbols_left_to_right[0], 0)
        for i in range(1, len(symbols_left_to_right)):
            prev_symbol = symbols_left_to_right[i - 1]
            prev_code = get_code(prev_symbol)
            prev_size = get_size(prev_symbol)
            curr_symbol = symbols_left_to_right[i]
            curr_size = get_size(curr_symbol)
            curr_code = (prev_code + 1) * 2 ** (curr_size - prev_size)
            set_code(curr_symbol, curr_code)

    def encode(self, data):
        """
        Encode given data.

        :param data: sequence of symbols (e.g. byte string, unicode string, list, iterator)
        :return: byte string
        """
        # TODO: change this to ''.join() for Python 2.7
        return bytes(self.encode_streaming(data))

    def encode_streaming(self, data_list):
        """
        This encoding schemes handles multi-char symbols
        :param data:
        :return:
        """
        sorted_symbols = sorted(self._code_table.items(), key=lambda x: byte_count(x[0]), reverse=True)
        buffer = 0
        size = 0
        self._compressed_bitcount = 0
        for data_item in data_list:
            i = 0
            bits_written_for_word = 0
            while i < len(data_item):
                i_at_loop_start = i
                # match the longest string possible out of the symbols list
                for symbol_info in sorted_symbols:
                    # symbol_info -> symbol, (bitsize, value)
                    symbol = symbol_info[0]
                    if data_item.startswith(symbol, i):
                        bitsize, value = symbol_info[1]
                        i += len(symbol)
                        self._compressed_bitcount += bitsize
                        # Shift new bits in the buffer
                        buffer = (buffer << bitsize) | value
                        size += bitsize
                        bits_written_for_word += bitsize
                        while size >= 8:
                            size -= 8
                            byte = buffer >> size
                            yield byte
                            buffer = buffer - (byte << size)
                assert i > i_at_loop_start, "'{}' at index {} was not found in the symbol list!".format(data_item[i], i)
            self.compressed_bits_map[data_item] = bits_written_for_word
        if size > 0:
            assert size < 8, "There should less than 1 byte remaining at this stage"
            byte = buffer << (8 - size)
            yield byte

    def decode(self, compressed_data, compressed_bitcount, split_into_array):
        self._compressed_bitcount = compressed_bitcount
        if split_into_array:
            return self.decode_split(compressed_data)
        else:
            return self._concat(self.decode_streaming(compressed_data))

    def decode_streaming(self, compressed_data):
        curr_idx = 0
        while curr_idx < self._compressed_bitcount:
            next_symbol, bits_read = self.read_symbol(compressed_data, curr_idx, self._compressed_bitcount)
            curr_idx += bits_read
            yield next_symbol

    def decode_split(self, compressed_data):
        result = []
        curr_idx = 0
        curr_string = ''
        while curr_idx < self._compressed_bitcount:
            next_symbol, bits_read = self.read_symbol(compressed_data, curr_idx, self._compressed_bitcount)
            curr_idx += bits_read
            curr_string += next_symbol
            if next_symbol == '\0':
                result.append(curr_string)
                curr_string = ''
        if curr_string:
            result.append(curr_string)
        return result

    def get_compressed_bitcount(self):
        return self._compressed_bitcount

    def get_concat(self):
        return self._concat

    def read_symbol(self, compressed_data, curr_idx, compressed_bitcount):
        remaining_bits = compressed_bitcount - curr_idx
        bits_to_read = min(self.tree_height, remaining_bits)
        val_N_to_find = get_bits_in_range(compressed_data, curr_idx, curr_idx + bits_to_read)
        num_missing_bits = self.tree_height - bits_to_read
        val_N_to_find <<= num_missing_bits  # shift to add in the missing bits from the end of the bit stream if necessary
        # find l by binary search on the lengths to match the range
        symbol_length = self._find_length(val_N_to_find)
        scaled_N = int(val_N_to_find / (2 ** (self.tree_height - symbol_length)))
        symbol_index = self.first_symbol_index_of_length[symbol_length - 1] + scaled_N - \
                       self.first_symbol_code_of_length[symbol_length - 1]
        symbol = self.symbols_left_to_right[symbol_index]
        return symbol, symbol_length

    def _find_length(self, value_to_seek):
        # binary search
        start = 0
        end = self.tree_height
        while end - start > 0:
            middle = (start + end) // 2
            range_result = self._value_is_in_range(value_to_seek, middle)
            if range_result == 'within_range':
                start = middle
                end = middle
            elif range_result == 'below_range':
                start = middle
            else:
                end = middle
        assert self._value_is_in_range(value_to_seek, start) == 'within_range'
        return start + 1

    def _value_is_in_range(self, value_to_seek, length_index):
        lower_code = self.first_symbol_code_of_length[length_index]
        upper_code = self.first_symbol_code_of_length[length_index + 1] if length_index < (self.tree_height - 1) \
            else (2 ** 63 - 1)
        length = length_index + 1
        scaled_val_to_seek = value_to_seek // 2 ** (self.tree_height - length)
        lower_limit = lower_code
        upper_limit = upper_code // 2
        if scaled_val_to_seek >= lower_limit:
            if scaled_val_to_seek < upper_limit:
                range_result = 'within_range'
            else:
                range_result = 'below_range'
        else:
            range_result = 'above_range'
        return range_result

    def get_symbols(self):
        return list(self.symbols_left_to_right)

    def calc_size(self):
        num_bytes_for_symbols, symbol_offset_table_size = \
            calc_indexed_symbols_bytes({symbol: bitsize for symbol, (bitsize, _) in self._code_table.items()})
        # Apart from the symbols table we just need to transmit the first indices and
        # codes. The codes could be stored very compactly since we know that
        # bits_for(Codes[i]) = i + 1, so we could work out an index progressively. However,
        # since this is likely to be small compared to the rest of the codec we take the simple
        # path for now of assuming they'll be stored in a bit array whose elements are the
        # size of the longest code
        first_codes_size = bytes_needed_for_num_bits(self.tree_height * self.tree_height)
        first_indices_size = bytes_needed_for_num_bits(
            self.tree_height * bits_for(len(self.first_symbol_index_of_length)))
        return num_bytes_for_symbols + symbol_offset_table_size + first_codes_size + first_indices_size

    def get_code_table(self):
        return self._code_table

    def print_code_table(self, verbose=False):
        def format_symbol(symbol):
            if symbol == '\n':
                return '\\n'
            elif symbol == '\0':
                return '\\0'
            else:
                return symbol

        print("Number of Symbols: {}".format(len(self.symbols_left_to_right)))
        if verbose:
            print("Symbols: ")
            for idx, symbol in enumerate(self.symbols_left_to_right):
                print("{}: '{}'".format(idx, format_symbol(symbol)))
        print(self.first_symbol_index_of_length)
        print(self.first_symbol_code_of_length)

    @classmethod
    def from_frequencies(cls, frequencies, concat=None):
        """
        Build Huffman code table from given symbol frequencies
        :param frequencies: symbol to frequency mapping
        :param concat: function to concatenate symbols
        """
        concat = concat or _guess_concat(next(iter(frequencies)))

        # Heap consists of tuples: (frequency, [list of tuples: (symbol, (bitsize, value))])
        heap = [(f, [(s, (0, 0))]) for s, f in frequencies.items()]

        # Use heapq approach to build the encodings of the huffman tree leaves.
        heapify(heap)
        while len(heap) > 1:
            # Pop the 2 smallest items from heap
            a = heappop(heap)
            b = heappop(heap)
            # Merge nodes (update codes for each symbol appropriately)
            merged = (
                a[0] + b[0],
                [(s, (n + 1, v)) for (s, (n, v)) in a[1]]
                + [(s, (n + 1, (1 << n) + v)) for (s, (n, v)) in b[1]]
            )
            heappush(heap, merged)

        # Code table is dictionary mapping symbol to (bitsize, value)
        table = dict(heappop(heap)[1])
        return cls(table, concat)

    @classmethod
    def from_data(cls, data):
        """
        Build Canonical Huffman code table from symbol sequence

        :param data: sequence of symbols (e.g. byte string, unicode string, list, iterator)
        :return: CanonicalCoder
        """
        if isinstance(data, list):
            data_to_count = [x for sublist in data for x in sublist]
        else:
            data_to_count = data
        frequencies = collections.Counter(data_to_count)
        return cls.from_frequencies(frequencies, concat=_guess_concat(data_to_count))


def calc_codec_size_as_canonical(non_canonical_codec):
    canonical_codec = CanonicalCodec(non_canonical_code_table=non_canonical_codec)
    return canonical_codec.calc_size()
