import collections

from common import calculate_markov_mapping
from compression.canonical_codec import CanonicalCodec
from compression.common import (bits_for,
                                bytes_needed_for_num_bits,
                                byte_count)


class MarkovCodec(object):
    """
    The MarkovCodec relies on the data having many high probability co-locations, e.g.,
    t -> h (in English). The idea is to find a limited number of very high probability
    co-locations. This codec doesn't determine if we have saved memory by this, but it
    does include an estimate of how much memory will be required for the chosen options.
    Compression is as follows:
       Look at context_depth chars. If the next char is one of the high probability
       next_chars (num_next_chars_to_store), record its index in the stream (typically a 1, 2 or 3 bit code).
       Else, store the special index and then record the actual symbol code. Note that
       pretty obviously this would need to save a lot of bits for the imposition of this
       special bit code.
    """

    def __init__(self, data_to_compress, context_depth=1):
        """
        In the following:
        - S is the number of symbols in the incoming data (not the number of symbols in the canonical huffman codec)
        - d is the context depth, i.e., the number of preceding characters to use to predict the next character
        - n is the number of characters in the compressed text
        The compression scheme is:
        - calculate the full next_chars map for the whole data set, sorting each next_chars list by frequency
          so that the most frequent items appear first in the list
        - replace the characters in each string with the matching index in the next_chars list
        - compress this markov chain index data with a canonical huffman coding

        In order to support efficient decompression we need to:
        - sort the symbols list so that first all the single byte characters appear, then 2 byte, then 3
        - record the index of the first 2 byte and the first 3 byte symbol (F2BI, F3BI below)
        - store this data in a byte array
        - sort the markov chains so that they are in the same order as the corresponding symbol indices
        - Now to create the Markov Chain data for transmission:
          * next_chars_lists = []
          * next_chars_list_offsets[S^d]
          * for each sorted markov chain element (a symbol + next_chars list pair):
              next_chars_list_offsets[i] = len(next_chars_list)
              add next_chars list to next_chars_lists

        We need to transmit:
        - The sorted symbols data (i.e., grouped by the character length in bytes)
        - the next_chars_list_offsets = S^d * bits_for(len(next_chars_lists)) bytes
        - the next_chars_lists data which has an element size of bits_for(S)
        - the compressed data
        - the canonical huffman coding tables

        The decompression scheme is:
        - context <- [SymbolIndexFor('\0' * d)]
        - output <- ""
        - while there are bits to process:
            markov_index <- CanonicalCodec.read_symbol()
            next_chars_list = next_chars_lists[next_chars_list_offsets[context]]
            symbol_index <- next_chars_list[markov_index]
            next_symbol <- SymbolByteOffset(symbol_index)
            context <- context + symbol_index % d
            output <- output + next_symbol
            if next_symbol == '\0'
              return output

        SymbolByteOffset is a piecewise linear bijective function which determines the byte offset for the symbol
        in the symbol data chunk given a symbol index as follows:
        SymbolByteOffset(i):
        - i is in the set [0, S)
        - return i, i < F2BI
        - return (i - F2BI) * 2 + F2BI, i is in the set [F2BI, F3BI)
        - return (i - F3BI) * 3 + F2BI + 2 * F3BI, i > F3BI

        The performance of this algorithm per character read is:
        - canonical symbol lookup is O(log h), where h is the height of the equivalent huffman tree (i.e., the number of
          bits in the longest code). The height, h, in the worst case log n, so this is O(log(log n))
        - O(1) (constant time) for the markov chain lookup

        This gives an overall performance for a text of length n of O(n * log(log(n))).

        NOTE: The algorithm below doesn't yet fully reflect this design.

        :param context_depth: How many prior symbols to examine to find the most likely next symbol
        """
        self.symbols = sorted(set([char for data in data_to_compress for char in data]), key=lambda x: byte_count(x))
        self.symbol_index_bits = bits_for(len(self.symbols))
        self._compressed_bitcount = 0
        self.context_depth = context_depth
        data_list = data_to_compress if isinstance(data_to_compress, list) else [data_to_compress]
        self.markov_chains = calculate_markov_mapping(self.context_depth, self.symbols, data_list)
        self.max_next_chars_list_length = max([len(v) for v in self.markov_chains.values()])
        self.next_chars_list_index_bits = bits_for(self.max_next_chars_list_length)
        data_as_markov_indices = [self._string_to_markov_indices(x) for x in data_to_compress]
        self.symbol_codec = CanonicalCodec.from_data(data_as_markov_indices)

    def calc_size(self):
        # - The sorted symbols data (i.e., grouped by the character length in bytes)
        # - the next_chars_list_offsets = S^d * bits_for(len(next_chars_lists)) bytes
        # - the next_chars_lists data which has an element size of bits_for(S)
        # - the compressed data
        # - the canonical huffman coding tables
        symbol_codec_size = self.symbol_codec.calc_size()
        symbol_bytes = sum([byte_count(x) for x in self.symbols])
        # for the markov chain we store a concatenation of all the next_chars_lists and
        # an array of bit indices (in the same order as the symbols) into that concatenated
        # blob of data
        num_symbols = len(self.symbols)
        symbol_index_bits = bits_for(num_symbols)
        next_chars_list_bits = sum([len(v) for v in self.markov_chains.values()] * symbol_index_bits)
        next_chars_list_bytes = bytes_needed_for_num_bits(next_chars_list_bits)
        next_chars_data_index_bits = bits_for(next_chars_list_bits)
        if self.context_depth == 1:
            # use an array lookup, throw the context away
            additional_markov_table_entry_bits = 0
        else:
            # use binary search so we need to also store the context bits
            additional_markov_table_entry_bits = self.context_depth * symbol_index_bits
        markov_table_size_bytes = \
            bytes_needed_for_num_bits(len(self.markov_chains.values()) * (next_chars_data_index_bits + additional_markov_table_entry_bits))
        return symbol_codec_size + symbol_bytes + markov_table_size_bytes + next_chars_list_bytes

    def print_code_table(self, verbose=False):
        def get_chars(next_chars_list):
            following_chars = [(_sanitise(self.symbols[idx]), freq) for idx, freq in next_chars_list]
            return following_chars

        print("Canonical Huffman Symbols:")
        self.symbol_codec.print_code_table()
        print("Markov Chain Codec:")
        longest_next_chars = (None, [(0, -1)])
        for symbol, next_chars in self.markov_chains.items():
            if len(next_chars) > len(longest_next_chars[1]):
                longest_next_chars = (symbol, next_chars)
        if verbose:
            print("Most next_chars = {}: '{}' -> {}".format(len(longest_next_chars[1]),
                                                            _sanitise(longest_next_chars[0]),
                                                            get_chars(longest_next_chars[1])))
        else:
            print("Most next_chars = {}, following context '{}'".format(len(longest_next_chars[1]),
                                                                        _sanitise(longest_next_chars[0])))
        print("Num Symbols: {}".format(len(self.symbols)))
        symbols_per_len = {k: 0 for k in range(1, max([byte_count(x) for x in self.symbols]) + 1)}
        for idx, symbol in enumerate(self.symbols):
            symbols_per_len[byte_count(symbol)] += 1
            if verbose:
                symbol_to_print = _sanitise(symbol)
                print("{}: '{}'".format(idx, symbol_to_print))
        print("Bits needed for symbol index: {}".format(bits_for(sum([byte_count(x) for x in self.symbols]))))
        print("(Bits needed to select same-sized symbol group: {})".format(bits_for(len(symbols_per_len.keys()))))
        print("(Bits needed to index symbols within same-sized groups: {})".format(
            bits_for(max(symbols_per_len.values()))))
        print("(Bits needed for Markov Chain index: {})".format(bits_for(len(self.markov_chains))))
        print("Num Markov Chains: {}".format(len(self.markov_chains.items())))
        if verbose:
            for prev_chars, following_char_indices in self.markov_chains.items():
                print("'{}' -> [{}]{}".format(_sanitise(prev_chars),
                                              len(following_char_indices),
                                              get_chars(following_char_indices)))

    def get_compressed_bitcount(self):
        return self._compressed_bitcount

    def encode(self, data):
        """
        Encode given data.

        :param data: sequence of symbols (e.g. byte string, unicode string, list, iterator)
        :return: byte string
        """
        # TODO: change this to ''.join() for Python 2.7
        return bytes(self.encode_streaming(data))

    def encode_streaming(self, data_list):
        """
        :param data_list:
        :return:
        """
        buffer = 0
        size = 0
        self._compressed_bitcount = 0
        for data_item in data_list:
            markov_indices = self._string_to_markov_indices(data_item)
            for markov_index in markov_indices:
                bitsize, value = self.symbol_codec.get_code_table()[markov_index]
                buffer = (buffer << bitsize) | value
                self._compressed_bitcount += bitsize
                size += bitsize
                while size >= 8:
                    size -= 8
                    byte = buffer >> size
                    yield byte
                    buffer = buffer - (byte << size)
        if size > 0:
            assert size < 8, "There should less than 1 byte remaining at this stage"
            byte = buffer << (8 - size)
            yield byte

    def _string_to_markov_indices(self, string_to_convert):
        indices = []
        prefixed_string = '\0' * self.context_depth + string_to_convert
        for i in range(self.context_depth, len(prefixed_string)):
            # work out the markov index
            prev_chars = prefixed_string[i - self.context_depth:i]
            assert prev_chars in self.markov_chains, "Context '{}' not seen previously".format(prev_chars)
            next_chars_list = self.markov_chains[prev_chars]
            next_char = prefixed_string[i]
            markov_index = -1
            for idx, (next_chars_symbol_idx, _) in enumerate(next_chars_list):
                next_chars_symbol = self.symbols[next_chars_symbol_idx]
                if next_chars_symbol == next_char:
                    markov_index = idx
                    indices.append(markov_index)
                    break
            assert markov_index != -1, \
                "Next char '{}' not seen in this context '{}' while encoding '{}'".format(string_to_convert,
                                                                                          _sanitise(next_char),
                                                                                          _sanitise(prev_chars))
        return indices

    def decode(self, compressed_data, compressed_bitcount, split_into_array):
        self._compressed_bitcount = compressed_bitcount
        concat = self.symbol_codec.get_concat()
        if split_into_array:
            return self.decode_split(compressed_data)
        else:
            return concat(self.decode_streaming(compressed_data))

    def decode_streaming(self, compressed_data):
        curr_idx = 0
        # create the startup context
        context = collections.deque(maxlen=self.context_depth)
        for _ in range(self.context_depth):
            context.append('\0')

        compressed_bitcount = self._compressed_bitcount
        while curr_idx < compressed_bitcount:
            markov_index, bits_read = self.symbol_codec.read_symbol(compressed_data, curr_idx, compressed_bitcount)
            context_str = ''.join(context)
            next_symbol_index, _ = self.markov_chains[context_str][markov_index]
            next_symbol = self.symbols[next_symbol_index]
            context.append(next_symbol)
            curr_idx += bits_read
            yield next_symbol

    def decode_split(self, compressed_data):
        def fill_context():
            for _ in range(self.context_depth):
                context.append('\0')

        curr_idx = 0
        # create the startup context
        context = collections.deque(maxlen=self.context_depth)
        fill_context()
        result = []
        current_string = ''
        compressed_bitcount = self._compressed_bitcount
        while curr_idx < compressed_bitcount:
            markov_index, bits_read = self.symbol_codec.read_symbol(compressed_data, curr_idx, compressed_bitcount)
            context_str = ''.join(context)
            next_symbol_index, _ = self.markov_chains[context_str][markov_index]
            next_symbol = self.symbols[next_symbol_index]
            context.append(next_symbol)
            curr_idx += bits_read
            current_string += next_symbol
            if next_symbol == '\0':
                # add what we have so far as a new string in the array and
                # reset the context
                result.append(current_string)
                current_string = ''
                fill_context()
        if current_string:
            result.append(current_string)
        return result


def _sanitise(symbol_string):
    return symbol_string.replace('\n', '\\n').replace('\0', '\\0')
