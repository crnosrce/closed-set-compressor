import os
from collections import Counter
from math import ceil
from math import log2
from operator import itemgetter
from typing import Callable
from typing import List
from typing import NamedTuple
from typing import NewType
from typing import Tuple

from dahuffman import HuffmanCodec


class SubstringRecord(NamedTuple):
    text: str
    freq: int


StrFreqTuple = Tuple[str, int]
StringList = List[str]
SubstringList = List[SubstringRecord]
Codec = NewType('Codec', HuffmanCodec)
MetricFunc = Callable[[SubstringRecord], int]

ALL_DATA_KEY = 'all_file_data'


def load_text(filename):
    with open(filename, 'r') as f:
        file_contents = f.read()
        if not file_contents.endswith('\0'):
            file_contents += '\0'
        return [file_contents]


def load_csv(filename):
    file_data = []
    # the file is expected to contain lines of individual strings separated by carriage returns
    # some string may contain the character sequence ['\','n'] which should be converted into
    # '\n' chars when read in.
    with open(filename, 'r') as f:
        for line in f:
            line = line.replace('\\n', '\n').strip()
            if line:
                if not line.endswith('\0'):
                    line += '\0'
                file_data.append(line)
    return file_data


def load_files(dir, read_csvs, context_depth=1):
    def add_nulls(string):
        if read_csvs:
            return string
        else:
            return string + '\0' * context_depth  # this is a hack to make a share dictionary work on large

    ending_to_load = '.csv' if read_csvs else '.txt'
    loader_fn = load_csv if read_csvs else load_text
    loaded_files = {}
    for root, _, files in os.walk(dir):
        for file in files:
            if file.endswith(ending_to_load):
                loaded_files[file] = loader_fn(os.path.join(root, file))
    loaded_files[ALL_DATA_KEY] = [x for sublist in loaded_files.values() for x in sublist]
    return loaded_files


def byte_count(utf8_string):
    try:
        return len(utf8_string.encode('utf-8'))
    except AttributeError:
        return 1  # EOF symbol, presumably


def bits_for(size):
    return ceil(log2(size))


def get_top_duplicated_strings(data_to_compress, max_string_len=10):
    """
    This returns the most duplicated substrings in the given data_to_compress of at
    least 1 character (as we will get this character frequency separately).
    :param data_to_compress: A unicode string (UTF-8)
    :param max_string_len:
    :return:
    """
    if not isinstance(data_to_compress, list):
        data_to_compress = [data_to_compress]
    accumulator = Counter()
    for data_to_search in data_to_compress:
        if not max_string_len:
            max_string_len = len(data_to_compress)
        upper_string_len = min(max_string_len, len(data_to_search)) + 1
        for length in range(3, upper_string_len):
            print('Checking substrings of length {}'.format(length))
            for start in range(len(data_to_search) - length):
                accumulator[data_to_search[start:start + length]] += 1
    return accumulator.most_common()


def get_top_duplicated_strings_across_stringlists(search_list, max_string_len=10):
    """
    This returns the most duplicated substrings in the given search_list of at
    least 3 characters .
    :param search_list: A list of unicode strings (UTF-8)
    :param max_string_len:
    :return:
    """
    accumulator = Counter()
    for data_to_search in search_list:
        # print('Searching within string "{}" for common substrings'.format(data_to_search))
        if not max_string_len:
            max_string_len = len(data_to_search)
        upper_string_len = min(max_string_len, len(data_to_search))
        for length in range(3, upper_string_len + 1):
            # print('Checking substrings of length {}'.format(length))
            for start in range(len(data_to_search) - length):
                accumulator[data_to_search[start:start + length]] += 1
    return accumulator.most_common()


def total_compressed_size(codec, data, codec_size_calculator, compressor):
    # as a quick test assume that we transform the codec into a canonical
    # codec. The compressed data size is unchanged.
    codec_size = codec_size_calculator(codec)
    compressed = compressor(codec, data)
    compressed_size = len(compressed)
    return codec_size + compressed_size


def bytes_needed_for_num_bits(bit_count: int) -> int:
    return (bit_count + 7) // 8


def int_to_bytes(x: int) -> bytes:
    return x.to_bytes(bytes_needed_for_num_bits(x.bit_length), 'big')


def make_tree(codec_table):
    def get_bits_as_bool_array(num_sym_bits, code_val):
        bool_array_for_bits = []
        while code_val > 0:
            bool_array_for_bits.insert(0, True if code_val & 0x1 else False)
            code_val >>= 1
        while len(bool_array_for_bits) < num_sym_bits:
            bool_array_for_bits.insert(0, False)
        return bool_array_for_bits

    def make_node():
        return {'left': None, 'right': None, 'symbol': None}

    tree = make_node()
    num_nodes = 0
    for symbol, (bitsize, value) in codec_table.items():
        current_node = tree
        bool_array = get_bits_as_bool_array(bitsize, value)
        for bit in bool_array:
            branch = 'right' if bit else 'left'
            if not current_node[branch]:
                current_node[branch] = make_node()
                num_nodes += 1
            current_node = current_node[branch]
        # current_node is now the leaf node
        current_node['symbol'] = symbol
    assert num_nodes > len(codec_table.items())
    return num_nodes, tree


def count_prefix_nodes(codec_table):
    return make_tree(codec_table)[0]


def __recurse_tree_for_symbols_in_left_to_right_order(tree_node, symbols):
    # the tree node is a dictionary with keys 'left', 'right', 'symbol'
    if tree_node['symbol'] is not None:
        # leaf node found - add the symbol
        symbols.append(tree_node['symbol'])
    else:
        __recurse_tree_for_symbols_in_left_to_right_order(tree_node=tree_node['left'], symbols=symbols)
        __recurse_tree_for_symbols_in_left_to_right_order(tree_node=tree_node['right'], symbols=symbols)


def get_symbols_in_left_to_right_order(codec_table):
    _, tree = make_tree(codec_table=codec_table)
    symbols = []
    __recurse_tree_for_symbols_in_left_to_right_order(tree_node=tree, symbols=symbols)
    return symbols


def calc_indexed_symbols_bytes(symbols_map):
    def get_len(symbol_to_measure):
        try:
            return len(symbol_to_measure)
        except TypeError as te:
            return 0

    def symbol_bit_count(symbol_to_measure):
        # this estimates the size based on the assumption that
        # multi-char symbols can be compacted using the dictionary
        # as well
        symbol_len = get_len(symbol_to_measure)
        if symbol_len == 1:  # one char
            return 8 * byte_count(symbol_to_measure)
        elif symbol_len > 1:
            bit_count = 0
            for symbol_char in symbol_to_measure:
                num_bits = symbols_map[symbol_char]
                bit_count += num_bits
            bit_count += symbols_map[
                '\0'] if '\0' in symbols_map else 4  # assume the null terminator will be a frequent character
            return bit_count
        else:
            return 8

    num_symbols = len(symbols_map)
    all_same_size = False
    if isinstance(list(symbols_map.keys())[0], int):
        # assume they're all ints
        all_same_size = True
        biggest_int = max(symbols_map.keys())
        bits_for_biggest = bits_for(biggest_int)
        total_bits_for_symbols = bits_for_biggest * num_symbols
    else:
        total_bits_for_symbols = 0
        for symbol in symbols_map.keys():
            num_symbol_bits = symbol_bit_count(symbol)
            total_bits_for_symbols += num_symbol_bits
    num_symbol_bytes = bytes_needed_for_num_bits(total_bits_for_symbols)

    if all_same_size:
        # if they're all the same size then we won't need the extra level of indirection
        symbol_offset_table_size = 0
    else:
        # the symbols may be variable lengths but we handle this by storing
        # an array of offsets into the concatenated bits of the symbol
        # data (similar to the compressed data itself), one per symbol.
        # The symbol_offset can also tell us which symbols are compressed
        # just by transmitting an extra byte to tell us which index is the
        # first of the compressed symbols
        symbol_offset_table_size = bytes_needed_for_num_bits(num_symbols * bits_for(num_symbol_bytes))
    return num_symbol_bytes, symbol_offset_table_size


def huffman_dictionary_bytes(codec):
    table = codec.get_code_table()
    # the bits for the symbol index are purely whatever is required for the
    # symbol offset table
    num_bytes_for_symbols, symbol_offset_table_size = \
        calc_indexed_symbols_bytes({symbol: bitsize for symbol, (bitsize, _) in table.items()})

    # now we need to work out how many nodes will be in our tree. There's almost certainly
    # a simple and clever way to know this (for a start, it must be known during construction
    # of the codec) but I'll just practice reconstructing the tree for now
    num_nodes_in_huffman_tree = count_prefix_nodes(codec_table=table)

    # there are two ways to store this non-canonical tree. One is in an array where all the values
    # are either leaf nodes or contain indices into other nodes in the array. The array is a "bit array"
    # so we can just index through the bits as required. The number of elements in this array would be
    # num_nodes_in_huffman_tree. Each node would be a bitwise union of either a leaf node's symbol index,
    # or two pointers to the next level in the tree. The size of that node is therefore:
    bits_for_symbol_index = bits_for(len(table.items()))
    node_size_bits = 1 + max(bits_for_symbol_index, 2 * bits_for(num_nodes_in_huffman_tree))
    # the total size in memory is therefore:
    compact_huffman_tree_bytes = bytes_needed_for_num_bits(num_nodes_in_huffman_tree * node_size_bits)

    # the other way to store the array would be to just use a sequential layout array. This requires 2^N nodes,
    # where N is the length of the longest symbol, and each node is bits_for_symbol_index long
    longest_symbol_length = max([symbol_size_group for (symbol_size_group, _) in table.values()])
    sequential_huffman_tree = bytes_needed_for_num_bits(bits_for_symbol_index * 2 ** longest_symbol_length)

    size_of_huffman_tree = min(compact_huffman_tree_bytes, sequential_huffman_tree)
    # print('Use a {} representation for minimal size'.format(
    #     'compact' if compact_huffman_tree_bytes < sequential_huffman_tree else 'sequential'))

    return size_of_huffman_tree + num_bytes_for_symbols + symbol_offset_table_size


def get_bit_at(data, index):
    byte_index = index // 8
    bit_index = index - (byte_index * 8)
    try:
        byte = data[byte_index]
    except IndexError as ie:
        print("byte_index = {} has problem on data length {}".format(byte_index, len(data)))
        byte = 0
    shifted_byte = byte << bit_index
    return (shifted_byte & 0x80) >> 7


def get_bits_in_range(data, start_index, end_index):
    result = 0
    for i in range(start_index, end_index):
        result = (result << 1) | get_bit_at(data, i)
    return result


def calculate_markov_mapping(context_depth, symbols, data_list):
    """
    :param data_list: a list of strings which are to be compressed (used in determining the codec)
    """

    def find_symbol_index(char):
        for idx, symbol in enumerate(symbols):
            if char == symbol:
                return idx
        assert False, "Character '{}' not found in symbols".format(char)

    # for each string in data_list, create tuples of (N prev chars, next char)
    # prev_to_next_tuples = [(x[0], x[1]) for data in data_list for x in data]
    # Note that we also need to account for the initial strings, for which the context
    # is a series of NULLS of length context_depth. In other words, the string is effectively
    # [NULL] * context depth, string_to_compress. It should be safe to use '\0' for this purpose.
    num_chars_to_examine = context_depth + 1
    accumulator = Counter()
    for data in data_list:
        full_context_data = '\0' * context_depth + data
        num_chars = len(full_context_data)
        assert num_chars >= num_chars_to_examine, "Full context data should always be available by design"
        for i in range(context_depth, num_chars):
            prev_to_next_tuple = (full_context_data[(i - context_depth):i], full_context_data[i])
            accumulator[prev_to_next_tuple] += 1
    next_chars_pairs = accumulator.most_common()

    markov_chains = {k: [] for (k, _), _ in next_chars_pairs}
    for (prev_chars, pred_char), frequency in next_chars_pairs:
        pred_char_symbol_index = find_symbol_index(pred_char)
        markov_chains[prev_chars].append((pred_char_symbol_index, frequency))
    # We sort the pred_char lists so that the most frequent values come first.
    # This is an important step that clusters the values around 0.
    return {k: sorted(v, key=itemgetter(1), reverse=True) for k, v in markov_chains.items()}