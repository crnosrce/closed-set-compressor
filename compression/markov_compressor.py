from collections import Iterable

from compression.canonical_codec import CanonicalCodec
from compression.common import total_compressed_size
from compression.markov_codec import MarkovCodec


def markov_dictionary(data_to_compress, context_depth):
    codec = MarkovCodec(data_to_compress, context_depth)
    return codec


def markov_dictionary_bytes(codec):
    return codec.calc_size()


def markov_compress(codec, data_to_compress):
    return codec.encode(data_to_compress)


def markov_decompress(codec, data_to_decompress, split=True):
    decompressed = codec.decode(data_to_decompress, codec.get_compressed_bitcount(), split)
    if split:
        return decompressed
    else:
        return ''.join(decompressed)


def markov_size(codec, data):
    total_size = total_compressed_size(codec=codec, data=data,
                                       codec_size_calculator=markov_dictionary_bytes,
                                       compressor=markov_compress)
    return total_size
