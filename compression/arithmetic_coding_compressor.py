from collections import Iterable


from compression.arithmetic_codec import ArithmeticCodec
from compression.common import total_compressed_size, bytes_needed_for_num_bits


def arithmetic_coding_dictionary_from_frequencies(frequencies):
    codec = ArithmeticCodec.from_frequencies(frequencies)
    return codec


def arithmetic_coding_dictionary(data_to_compress):
    if isinstance(data_to_compress, Iterable):
        data_to_compress = ''.join(data_to_compress)
    codec = ArithmeticCodec.from_data(data_to_compress)
    return codec


def arithmetic_coding_dictionary_bytes(codec):
    return codec.calc_size()


def arithmetic_coding_compress(codec, data_to_compress):
    compressed = codec.encode(data_to_compress)
    # for string, bit_count in codec.compressed_bits_map.items():
    #     print("{}: {}".format(string, bit_count))
    # total_bits = sum(codec.compressed_bits_map.values())
    # print("Total bits/bytes: {}/{}".format(total_bits, bytes_needed_for_num_bits(total_bits)))
    # print("Longest compressed string in bits = {}".format(max(codec.compressed_bits_map.values())))
    return compressed


def arithmetic_coding_decompress(codec, data_to_decompress):
    decompressed = codec.decode(data_to_decompress)
    return decompressed


def arithmetic_coding_size(codec, data):
    total_size = total_compressed_size(codec=codec, data=data,
                                       codec_size_calculator=arithmetic_coding_dictionary_bytes,
                                       compressor=arithmetic_coding_compress)
    return total_size
