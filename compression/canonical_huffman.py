from collections import Iterable


from compression.canonical_codec import CanonicalCodec
from compression.common import total_compressed_size, bytes_needed_for_num_bits


def canonical_dictionary_from_frequencies(frequencies):
    codec = CanonicalCodec.from_frequencies(frequencies)
    return codec


def canonical_dictionary(data_to_compress):
    if isinstance(data_to_compress, Iterable):
        data_to_compress = ''.join(data_to_compress)
    codec = CanonicalCodec.from_data(data_to_compress)
    return codec


def canonical_dictionary_bytes(codec):
    return codec.calc_size()


def canonical_compress(codec, data_to_compress):
    compressed = codec.encode(data_to_compress)
    # for string, bit_count in codec.compressed_bits_map.items():
    #     print("{}: {}".format(string, bit_count))
    # total_bits = sum(codec.compressed_bits_map.values())
    # print("Total bits/bytes: {}/{}".format(total_bits, bytes_needed_for_num_bits(total_bits)))
    # print("Longest compressed string in bits = {}".format(max(codec.compressed_bits_map.values())))
    return compressed


def canonical_decompress(codec, data_to_decompress, split=True):
    decompressed = codec.decode(data_to_decompress, codec.get_compressed_bitcount(), split)
    if split:
        return decompressed
    else:
        return ''.join(decompressed)


def canonical_size(codec, data: str) -> int:
    total_size = total_compressed_size(codec=codec, data=data,
                                       codec_size_calculator=canonical_dictionary_bytes,
                                       compressor=canonical_compress)
    return total_size
